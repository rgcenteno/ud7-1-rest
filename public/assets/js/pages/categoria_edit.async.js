$(document).ready(function(){
   makeRequest();
});

function makeRequest(){
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
      // Get the value of "some_key" in eg "https://example.com/?some_key=some_value"
      let id_categoria = params.id_categoria; // "some_value"

    var jqxhr = $.get( "index.php", {'controller' :'CategoriaAsync', 'action' : "editAsync", 'id_categoria' : id_categoria}, function(data) {        
        
    })    
    .done(function(data) {        
        json = jQuery.parseJSON(data);
        console.log(json);
        $("#titulo").text(json.titulo);
        
        var id_categoria = document.getElementById('id_categoria');
        id_categoria.setAttribute('value', json.categoria.id_categoria);
        
        var inputNombre = document.getElementById('nombre');
        inputNombre.setAttribute('value', json.categoria.nombre_categoria);
        
        var bread = document.getElementById('breadcumb');
        for (const key in json.breadcumb) {
            var aElement = document.createElement('a');
            aElement.setAttribute('href', json.breadcumb[key].url);
       
            aElement.appendChild(document.createTextNode(key));
            var liElement = document.createElement('li');
            if(json.breadcumb[key].active){
                liElement.setAttribute('class', 'breadcrumb-item active');
            }
            else{
                liElement.setAttribute('class', 'breadcrumb-item');
            }            
            liElement.appendChild(aElement);
            bread.appendChild(liElement);            
        }
        json.categoriasList.forEach(optionSelect, json.idPadre);
        //json.data.forEach(rowCategoria);
    })
    .fail(function(response) {   
        debugger;
        alert( "Error al realizar la petición: " + status + ": " + response.statusText );
    })
    .always(function() {
        
    }); 
}

function showBreadcumb(key, element){
    var breadcumb = document.getElementById('breadcumb');
    breadcumb.appendChild(document.createTextNode(key));
}

function optionSelect(categoria, idSelected){
    //debugger;
    console.log(categoria);
    var select = document.getElementById('id_padre');
    var option = document.createElement('option');
    option.setAttribute('value', categoria.id_categoria);
    if(categoria.id_categoria === idSelected){
        option.setAttribute('selected', 'selected');
    }
    option.appendChild(document.createTextNode(getFullPath(categoria)));
    select.appendChild(option);
}

function getFullPath(categoria){
    //debugger;
    var fullPath = categoria.nombre_categoria;
    var padre = categoria.padre;
    while(padre !== undefined){
        fullPath = padre.nombre_categoria + " > " + fullPath;
        padre = padre.padre;
    }
    return fullPath;
}