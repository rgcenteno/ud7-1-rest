$(document).ready(function(){
   makeRequest();
});

function makeRequest(){
 
}
    var jqxhr = $.get( "index.php", {'controller' :'CategoriaAsync', 'action' : "indexData"}, function(data) {        
        
    })    
    .done(function(data) {        
        json = jQuery.parseJSON(data);
        console.log(json);
        $("#titulo").text(json.titulo);
        var bread = document.getElementById('breadcumb');
        for (const key in json.breadcumb) {
            var aElement = document.createElement('a');
            aElement.setAttribute('href', json.breadcumb[key].url);
       
            aElement.appendChild(document.createTextNode(key));
            var liElement = document.createElement('li');
            if(json.breadcumb[key].active){
                liElement.setAttribute('class', 'breadcrumb-item active');
            }
            else{
                liElement.setAttribute('class', 'breadcrumb-item');
            }            
            liElement.appendChild(aElement);
            bread.appendChild(liElement);            
        }
        json.data.forEach(rowCategoria);
        var deleteButtons = document.getElementsByClassName('btn-delete');
        for (var i = 0; i < deleteButtons.length; i++) {
            deleteButtons[i].addEventListener('click', deleteRow, false);
        }
    })
    .fail(function(response) {   
        alert( "Error al realizar la petición: " + status + ": " + response.statusText );
    })
    .always(function() {
        
    }); 



function deleteRow(click){
    var id_categoria = this.getAttribute('data-id_categoria');
    $.ajax({
        method: "DELETE",
        url: "./?controller=categoriaAsync",
        data: { 'id_categoria' : id_categoria }
   })
  .done(function( msg ) {
      if(msg.borrado){
          guiDeleteRow(msg.id_categoria);
      }
      else{
          alert('No se ha podido eliminar la fila');
      }
  })
  .fail(function(msg){
      alert( "Error al realizar el borrado: " + status + ": " + response.statusText );
  });
}

async function guiDeleteRow(id_categoria){
    var tr = document.getElementById('tr-'+id_categoria);
    tr.setAttribute('class', 'table-danger');
    await sleep(1000);
    tr.remove();
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function showBreadcumb(key, element){
    var breadcumb = document.getElementById('breadcumb');
    breadcumb.appendChild(document.createTextNode(key));
}

function rowCategoria(categoria){
    var tbodyRef = document.getElementById('categoriaTable').getElementsByTagName('tbody')[0];
    var newRow = tbodyRef.insertRow();
    newRow.setAttribute('id', 'tr-' + categoria.id_categoria);

    // Insert a cell at the end of the row
    var newCell = newRow.insertCell();
    // Append a text node to the cell
    var nombre = document.createTextNode(categoria.nombre_categoria);
    newCell.appendChild(nombre);
    
    var cellPadre = newRow.insertCell();
    var nombrePadre;
    if(categoria.padre !== undefined){
        nombrePadre = document.createTextNode(categoria.padre.nombre_categoria);
    }
    else{
        nombrePadre = document.createTextNode('-');
    }
    cellPadre.appendChild(nombrePadre);
    
    var fullPath = categoria.nombre_categoria;
    var padre = categoria.padre;
    while(padre !== undefined){
        fullPath = padre.nombre_categoria + " > " + fullPath;
        padre = padre.padre;
    }
    
    var cellRuta = newRow.insertCell();
    var nombrePadre = document.createTextNode(fullPath);
    cellRuta.appendChild(nombrePadre);
    
    var cellOptions = newRow.insertCell();
    cellOptions.setAttribute('align', 'center');
    var aEdit = document.createElement('a');
    aEdit.setAttribute('class', 'btn btn-clock btn-outline-primary');
    aEdit.setAttribute('href', './?controller=categoriaAsync&id_categoria='+categoria.id_categoria);
    var iEdit = document.createElement('i');
    iEdit.setAttribute('class', 'fas fa-edit');
    aEdit.appendChild(iEdit);
    
    var aDelete = document.createElement('a');
    aDelete.setAttribute('class', 'btn btn-clock btn-outline-danger btn-delete');
    aDelete.setAttribute('data-id_categoria', categoria.id_categoria);
    aDelete.setAttribute('href', '#'); //'./?controller=categoriaAsync&action=delete&id_categoria='+categoria.id_categoria
    var iDelete = document.createElement('i');
    iDelete.setAttribute('class', 'fas fa-trash');
    aDelete.appendChild(iDelete);
    
    //var nombrePadre = document.('<a class="btn btn-clock btn-outline-primary" href="./?controller=categoria&action=edit&id_categoria='+categoria.id_categoria+'"><i class="fas fa-edit"></i></a> <a class="btn btn-clock btn-outline-danger" href="./?controller=categoria&action=delete&id_categoria='+categoria.id_categoria+'"><i class="fas fa-trash"></i></a>');
    cellOptions.appendChild(aEdit);
    cellOptions.appendChild(document.createTextNode(' '));
    cellOptions.appendChild(aDelete);
}