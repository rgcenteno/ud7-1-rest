<?php

/* 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

namespace Com\Daw2\Controllers;

use \Com\Daw2\Helpers\Mensaje;
/**
 * Description of TestController
 *
 * @author rafa
 */
class CategoriaAsyncController extends \Com\Daw2\Core\BaseController{
   
    public function index(){
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            if(!isset($_GET['id_categoria']) || !filter_var($_GET['id_categoria'], FILTER_VALIDATE_INT)){
                $this->showIndex();
            }
            else{
                $this->showEdit();
            }
        }
        elseif($_SERVER['REQUEST_METHOD'] == 'DELETE'){
            $request_vars = self::getVars();
            if(isset($request_vars['id_categoria']) && filter_var($request_vars['id_categoria'], FILTER_VALIDATE_INT)){
                $this->deleteAsync($request_vars['id_categoria']);
            }
            else{               
                $this->badRequest('Required int id_categoria');
            }
        }
        elseif($_SERVER['REQUEST_METHOD'] == 'POST'){
            
        }
    }
    /**
     * Sin emulado obtenemos que los números se reciben como float
     */
    public function showIndex(?Mensaje $msg = NULL)
    {                                         
        $this->view->showViews(array('templates/header.view.php', 'categoria.async.view.php', 'templates/footer.view.php'), array('js' => array('assets/js/pages/categoria.async.view.js')));      
    }
    
    public function indexData()
    {     
        try{
            $_vars = array('titulo' => 'Categorías',
                          'breadcumb' => array(
                            'Inicio' => array('url' => '#', 'active' => false),
                            'Categorias' => array('url' => '#','active' => true))                     
                );
            $model =  new \Com\Daw2\Models\CategoriaModel();      
            $_vars["data"] = $model->getAllCategorias();
            //var_dump($_vars['data']);
            header("HTTP/1.1 200 OK");
            echo json_encode($_vars, TRUE);
        }
        catch(\PDOException $ex){
            header("HTTP/1.1 500 Error al acceder a BBDD");
        }
        catch(\Exception $ex){
            header("HTTP/1.1 500 Error desconocido");
        }
    }
    
    public function badRequest(string $text){
        $_vars = array(            
            'status' => 'HTTP/1.1 400 '.$text
        );
        $this->view->show('api.view.php', $_vars);
    }
    
    public function deleteAsync(int $id){
        $categoriaModel = new \Com\Daw2\Models\CategoriaModel();
        $borrado = $categoriaModel->deleteCategoria($id);
        $response = array(
            'id_categoria' => $id,
            'borrado' => $borrado
        );
        $_vars = array(            
            'status' => 'HTTP/1.1 200 OK',
            'response' => $response
        );
        $this->view->show('api.view.php', $_vars);
    }
    
    public function insertCategoria(){
        try{
            if(isset($_GET['id_padre'])){
                $idPadre = filter_var($_GET['id_padre'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                if(!is_null($idPadre) && $idPadre <= 0){
                    $idPadre = NULL;
                }               
            }
            else{
                $idPadre = NULL;
            }
            if(!isset($_GET['nombre']) ||empty($_GET['nombre'])){            
                throw new \Exception("Se debe insertar el nombre de la categoría");
            }        
            else{
                $_vars = array('titulo' => 'Insertar categoría');
                $nombre = filter_var($_GET['nombre'], FILTER_SANITIZE_STRING);
                $_vars['nombre'] = $nombre;

                $model = new \Com\Daw2\Models\CategoriaModel();
                $_vars['id'] = $model->insertCategoria($nombre, $idPadre);            

                $this->view->showViews(array('templates/header.view.php', 'categoria.insert.view.php', 'templates/footer.view.php'), $_vars);
            }        
        }
        catch(\PDOException $ex){
            header("HTTP/1.1 500 Error al acceder a BBDD");
        }
        catch(\Exception $ex){
            header("HTTP/1.1 500 Error desconocido");
        }
    }    
    
    public function insertCategoriaObject(){
        if(isset($_GET['id_padre'])){
            $idPadre = filter_var($_GET['id_padre'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            if(!is_null($idPadre) && $idPadre <= 0){
                $idPadre = NULL;
            }               
        }
        else{
            $idPadre = NULL;
        }
        if(!isset($_GET['nombre']) ||empty($_GET['nombre'])){            
            throw new \Exception("Se debe insertar el nombre de la categoría");
        }        
        else{
            $_vars = array('titulo' => 'Insertar categoría Object');
            $nombre = filter_var($_GET['nombre'], FILTER_SANITIZE_STRING);
            $_vars['nombre'] = $nombre;
            
            $model = new \Com\Daw2\Models\CategoriaModel();
            $categoria = new \Com\Daw2\Helpers\Categoria(NULL, is_null($idPadre) ? NULL : $model->loadCategoria($idPadre), $nombre);
                        
            $_vars['categoria'] = $model->insertCategoriaObject($categoria);            
            
            $this->view->showViews(array('templates/header.view.php', 'categoria.insert.view.php', 'templates/footer.view.php'), $_vars);
        }        
        
    }
    
    public function new(){
        $model = new \Com\Daw2\Models\CategoriaModel();
        if(!isset($_POST['action'])){
            $categoria = \Com\Daw2\Helpers\Categoria::getStdClass();
            $categoriasList = $model->getAllCategorias();
            //var_dump($categoriasList);
            $_vars = array(
                'titulo' => 'Alta categoría', 
                'categoria' => $categoria,
                'categoriaEdit' => $categoria,
                'idPadre' => !is_null($categoria->padre) ? $categoria->padre->id : NULL,
                'categoriasList' => $categoriasList,
                'breadcumb' => array(
                    'Inicio' => array('url' => '#', 'active' => false),
                    'Categoria' => array('url' => '?controller=categoria','active' => false),
                    'Nueva' => array('url' => '#', 'active' => true))
                );
            $this->view->showViews(array('templates/header.view.php', 'categoria.edit.view.php', 'templates/footer.view.php'), $_vars);
        }
        elseif($_POST['action'] == 'guardar'){
            $_errors = $this->checkForm($_POST, false);
            
            if(count($_errors) === 0){
                $padre = ($_POST['id_padre'] > 0) ? $model->loadCategoria($_POST['id_padre']) : NULL;
                $categoria = new \Com\Daw2\Helpers\Categoria(NULL, $padre, filter_var($_POST['nombre'], FILTER_SANITIZE_SPECIAL_CHARS));
                $categoria = $model->insertCategoriaObject($categoria);                 
                $this->index();
            }
            else{                                
                $std = $this->sanitizeForm($_POST);
                $categoriasList = $model->getAllCategorias();
                //var_dump($categoriasList);
                $_vars = array(
                    'titulo' => 'Alta categoría' , 
                    'categoria' => $std,
                    'categoriaEdit' => $std,
                    'errors' => $_errors,
                    'idPadre' => isset($std->id_padre) ? $std->id_padre : NULL,
                    'categoriasList' => $categoriasList,
                    'breadcumb' => array(
                        'Inicio' => array('url' => '#', 'active' => false),
                        'Categoria' => array('url' => '?controller=categoria','active' => false),
                        'Nueva' => array('url' => '#', 'active' => true))
                    );

                $this->view->showViews(array('templates/header.view.php', 'categoria.edit.view.php', 'templates/footer.view.php'), $_vars);
            }
        }
    }
    
    public function editAsync(){
        $model = new \Com\Daw2\Models\CategoriaModel();
        if(isset($_GET['id_categoria'])){
            $idCategoria = filter_var($_GET['id_categoria'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            if(!is_null($idCategoria)){                
                $categoria = $model->loadCategoria($idCategoria);
                $categoriasList = $model->getAllCategorias();
                //var_dump($categoriasList);
                $_vars = array(
                    'titulo' => 'Editar categoría: '.self::getFullName($categoria) , 
                    'categoria' => $categoria,
                    'idPadre' => isset($categoria['id_padre']) ? $categoria['id_padre'] : 0,
                    'categoriasList' => $categoriasList,                    
                    'breadcumb' => array(
                        'Inicio' => array('url' => '#', 'active' => false),
                        'Categoria' => array('url' => '?controller=categoria','active' => false),
                        'Editar' => array('url' => '#', 'active' => true)),                    
                    );
                header("HTTP/1.1 200 OK");
                echo json_encode($_vars, TRUE);
            }
            else{
                //Si la petición es incorrecta o han dado a cancelar, recargamos el listado
                header("HTTP/1.1 400 Bad Request");                
            }
        }
    }
    
    private static function getFullName($categoria){
        $res = $categoria['nombre_categoria'];
        $actual = isset($categoria['padre']) ? $categoria['padre'] : NULL;
        while($actual != NULL){
            $res = $actual['nombre_categoria'] . " > ". $res;
            $actual = isset($actual['padre']) ? $actual['padre'] : NULL;
        }
        return $res;
    }
    
    public function showEdit(){        

        $this->view->showViews(array('templates/header.view.php', 'categoria.edit.view.php', 'templates/footer.view.php'), array('js' => array('assets/js/pages/categoria_edit.async.js')));            
    }
    
    public function delete() : void{
        $model = new \Com\Daw2\Models\CategoriaModel();
        $id = filter_var($_GET['id_categoria'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if(!is_null($id)){
            try{
                if($model->deleteCategoria($id)){
                    $this->index(new Mensaje("success", "¡Eliminada!", "Categoría borrada con éxito"));
                }
                else{
                    $this->index(new Mensaje("warning", "Sin cambios", "No se ha borrado la categoría: $id"));
                }            
            }
            catch(\PDOException $ex){
                if(strpos($ex->getMessage(), '1451') !== false){
                    $this->index(new Mensaje("danger", "No permitido", "Antes de borrar una categoría debe editar o borrar todas las categorías hijas."));
                }
                else{
                    $this->index(new Mensaje("danger", "No permitido", "PDOException: ".$ex->getMessage()));
                }
            }
        }
        else{
            $this->index(new Mensaje("warning", "Petición incorrecta", "No se ha facilitado la categoría a borrar"));
        }
    }
    
    private function checkForm(array $_data, bool $isEditing = true) : array{
        $model = new \Com\Daw2\Models\CategoriaModel();
        $_errors = [];
        $idPadre = filter_var($_data['id_padre'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if(is_null($idPadre)){
            $_errors['id_padre'] = 'Ha insertado un valor inválido como categoría padre.';
        }
        else{
            if($idPadre > 0){
                $categoriaPadre = $model->loadCategoria($idPadre);
                if(is_null($categoriaPadre)){
                    $_errors['id_padre'] = 'El padre seleccionado no existe.';
                }
            }
        }
        if(empty(trim($_data['nombre']))){
            $_errors['nombre'] = 'Inserte un nombre de categoría';
        }
        $idCategoria = filter_var($_data['id_categoria'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if($isEditing){
            if(is_null($idCategoria)){
                $_errors['name'] = "Error en la petición, inténtelo de nuevo";
            }
            else{
                $categoria = $model->loadCategoria($idCategoria);
                if(is_null($categoria)){
                    $_errors['name'] = "Error en la petición, la categoría no existe";
                }
            }
            if($idPadre === $idCategoria){
                $_errors['id_padre'] = 'La categoría no puede ser padre de si misma.';
            }
        }    
        return $_errors;
    }
    
    private function sanitizeForm(array $_data) : \stdClass{
        $element = new \stdClass();
        foreach($_data as $key => $value){
            $element->$key = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        $element->id = $element->id_categoria;
        return $element;
    }
}
