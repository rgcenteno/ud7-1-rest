<?php
namespace Com\Daw2\Core;

abstract class BaseController {
    protected $view;

    function __construct() {
        $this->view = new View(get_class($this));
    }
    
    function getModel(string $model){
        $config = \Com\Daw2\Core\Config::getInstance();       
        $modelName = $config->get('MODELS_NAMESPACE').$model;
        return new $modelName();
    }
    
    public static function getVars()
    {            
        $request_vars = array();
        if (0 === strlen(trim($content = file_get_contents('php://input'))))
        {
          $content = false;          
        }        
        else{
            parse_str($content, $request_vars );
        }
        return $request_vars;
    }
}